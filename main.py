import unittest
def cmp(a, b):
    return (a > b) - (a < b)

unittest.TestLoader.sortTestMethodsUsing = lambda _, x, y: cmp(y, x)

class Parkhomenko_Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        print("This is initialization code")

    def test1(self):
        print("Message...")

    def test2(self):
        self.assertTrue(True)
        print("always true")

    def test3(self):
        self.assertFalse(False)
        print("always false")

    @classmethod
    def tearDownClass(cls):
        print("This is cleaning code")

if __name__ == '__main__':

    unittest.main()
